let doneCb = [];
let failCb = [];
let isCalled = false;

class DoAsync {
  constructor (func) {
    function done(res) {
      if (isCalled) {
        return;
      }

      isCalled = true;
      let result = null
      setTimeout(() => { 
        doneCb.forEach((item) => {
          if(!result) {
            result = item(res)
          } else {
            result = item(result)
          }
        }); 
      }, 0);
    };

    function fail(err) {
      if (isCalled) {
        return;
      }

      isCalled = true;
      let result = null;
      setTimeout(() => { 
        failCb.forEach((item) => { 
          if(!result) {
            result = item(err);
          } else {
           result = item(result);
          }
         }); 
      }, 0);
    };

    func(done, fail);
  }

  after(cb) {
    doneCb.push(cb);
    return this;
  }

  error(cb) {
    failCb.push(cb);
    return this
  }
}
